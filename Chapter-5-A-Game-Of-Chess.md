# Chapter V
## A Game Of Chess

"I do not understand." said Chloe while a new board of chess was sitting in front of them. They were about to start a blitz. It seemed like Sheiny had found a full set of chess in Mr. Hambleton's shed, even with a proper chess clock. It was a time between the scenes. No pieces were yet moved from their positions. White pieces were on Chloe's side.

"What would be harmful in making software that is not useful, like games for example, to be not Free? It's a form of art after all." said Chloe while moving a pawn to E4. 

"Art has a different criteria, then software, yes..." said Sheiny, moving a knight to C6 "...all we want from art is it to be share-able." Chloe thought for a while and moved a knight to F3 without uttering a word. "Take for example VLC..." Sheiny continued "...it's Free Software to play, usually, non free files." Sheiny responded with a pawn on D5, revealing the queen. "It would be almost alright if, say, game data, textures and alike would be proprietary, but that the software would be free.". "The game engine?" asked Chloe while making her next move in chess. It was weird to her that Sheiny had revealed her queen. Anyways, Chloe moved a bishop to D3. "Not quite the game engine." Sheiny told Chloe when moving her second knight to F6 "A game engine is simply a collection of pre-made software to build games simpler. But the game developers always add more to it..." 

Chloe kept listening, silently taking Sheiny's pawn with her own at D5. Sheiny stopped talking. Chloe was first to take a piece. This surprised Sheiny a little bit. 

"What was I saying?..." Sheiny mumbled deciding on the next move. As a form of vicious revenge Sheiny took Chloe's pawn, the one that took hers a step earlier on D5, with non other but her majesty's queen her self. "The idea of a game engine is borrowed from Free Software it self..." Sheiny continued "...the problem is that Games are considered new software." 

"Not copylefted?" Chloe asked while moving a knight to C3 in hopes of taking the queen. 

"That's exactly what I'm talking about." said Sheiny while moving her queen away from Chloe's knight to H5. "Game Engines are almost like Free Software. Some game engines give you the four freedoms. Others don't, but the point is that you are using parts of it's object code to make more object code." Chloe, in some kind of panic, castled with the rook on the right. Moving the king to G1 and the rook to F1 in one move. "In a game made with a game engine, the source code would be both, the source of the engine and of the logic built on-top of the engine." Sheiny continued "The game art, if not the part of the game binary and interchangeable with different art has no meaning to the software of the game. You could substitute them with other images and still play the game." Sheiny stopped talking and moved a bishop to G4. 

"But what would it change if the logic is free or not?" asked Chloe while moving pawn to H3 in a desperate attempt to take out the bishop "People want to play the games unchanged, like they watch movies, or read books. What is the point here?". "Nonsense." said Sheiny while moving her knight to E5. 

"Absolute, nonsense..." Chloe agreed, she saw a possibility of taking Sheiny's knight with her own at this spot. But she didn't. She took the bishop that she intended to take earlier on G4 with a pawn. "I could've taken your knight." she said to Sheiny. Sheiny, instead of moving her knight out of there, added the second knight around it, on G4. Chloe looked all confused. Why didn't Sheiny moved her knight away from there? She didn't think much and took Sheiny's knight at E5 with her own. 

"Nonsense!" started Sheiny again "People do mods. People do alternative cuts of films. People write fan-fiction stories about their favourite characters. This freedom should exist." Sheiny moved the queen to H2. Checkmate! Chloe has lost the battle.

After a few pointless attempts at saving her king from the checkmate, Chloe tried to argue further. "*He* didn't care." she said "*He* was completely in the game for several hours, while it was the unaltered original version."

"*He* was an addicted imbecile." stated Sheiny "The game had probably a lot to do with it. Perhaps it was built with a team of talented psychologists. The art, the animation, the music, the loot boxes where all designed for one thing and one thing only, to induce as much addiction as possible. With a Free Game it's possible to alter it, so it's not addicting as much. It's possible to change anything at all with your own copy. The original will always be there. If people want to play the addictive version, they could install the original. Or they could install your version if you choose to share it. Or the original developer could agree with you and alter the original to include your changes. With Free Software it's all possible and it should be possible."

"But then..." Chloe started her last attempt "the original will be overwritten and no longer existing." 

"Well, if nobody keeps a copy of the original it could be the case" answered Sheiny without thinking much "... but for the most part all copies are stored somewhere. In git, for example, every change is recorded and back-traceable. No version and no version's source code is usually lost."

Chloe sat there, looking at the checkmate and thinking about computer games and the boy with long hair. Sheiny was thinking of Mendel. It had to be a week since she last saw him. Or maybe even two weeks. From the constant cycle of work and school it was very hard to tell for certain. His gaze was haunting her memories. Not stalking per say, but probably just looking at her with an unsure question of whether he knew her before. Thoughts of quantum physics passed through her mind. Mendel said that his long gone girlfriend was looking somewhat similarly to Sheiny. Or was it that Mendel was just trying to piece her image from Sheiny's? It didn't matter. For an instance a ludicrous thought of Mendel being a man from the future came into her imagination. From times when she is his girlfriend. Suddenly, some inexplicable event had to have brought him back in time to current days. He was looking in the same neighbourhood for the same girl. Finding Sheiny. A little girl, only slightly resembling his future girlfriend.

But of course it was all nonsense. A sweet dream, induced by inevitability that Mendel is somehow unchangeable in his love toward that mysterious woman. Sheiny thought to tell him that his girlfriend had died. Or that she stopped loving him. But she didn't even know who it was. And asking him this, or lying to him about her later, was too cruel for her. She killed a man, she thought. How is there more cruel than that? She was trying to calm herself by thinking that it all was just a mistake. That she didn't really want to kill him. That he deserved it somehow. But what's done is done. She wanted to owe to never carry a weapon again. But what if the circumstances will require it? She wanted to forget about the deed, but the deed was too strong to forget it. And if not the killing of that man, just a few minutes prior she nearly killed her best friend for envy.

"Am I a cruel bitch?" asked Sheiny, half crying. 

Chloe understood what Sheiny was referring to. She came closer and hugged her tight. "You saved my life." Chloe told her. 

"But I wanted to kill you prior." Seiny's tears came in full force, damping Chloe's Sholder. 

"You weren't. You weren't able to pull that trigger on me. You were like you are now when you pointed that at me. All boo hoo, crying little girl. There was no way you could shoot me. Yes. It was scary. But since that day, I am sure that you are trustworthy. You are a kind soul."

"I wanted to kill you for envy!"

"For love." answered Chloe "It was love. Not envy."

Sheiny seemed to have filled up with moral. They started filming again. After that Sheiny went not home. She walked a bit further towards the area of Mendel's. The investigation at that place was over. It was rumored that the police have accused some guy that was in frequent battles with that angry man. What happened with him is unknown. Probably he is walking free already. There was not a single evidence against him, she thought. But who knows for sure?

Sheiny was about to enter the building as her heart started pounding very rapidly. This place didn't seem right. She remembered the courage she had when filming with Chloe of the first time. And she opened the door. But the heart didn't stop pounding. She could not handle this place. Something about it was too scary. And it wasn't Mendel. A few floors above the place where she is now, she took a life of a man. The trauma came back to her. He was running at her full speed with a knife, she thought. It was self defense. It was okay, she thought. But nothing could stop the beating of her heart. So she left the building.

She remembered that Mendel told her his place of work, when they walked together the first time. It was a wood-chopping little factory right outside the city. It was far away. But it was way better than the damned building. So the next day after school she didn't go to the shed. She took a bus to Mendel's work instead. When she arrived at the location she was slightly disoriented. It was a very silent place full of tiny factories producing all kinds of rubbish. Many of those factories were closed and abandoned. But a few worked still. A handful of people on each of them were doing some tasks. And after two or three working factories she saw that wooden-chopping little factory that Mendel had talked about. Her heart started pounding again. This time she liked it. She stepped in and there he was.

He saw her almost immediately and didn't know what to think about it. An instance, he though, she was imaginary. Another man came towards her and asked "What are you doing here, child?". "It's my daughter" said Mendel. Sheiny was amazed by his wit. By his ability to lie so quickly and perfectly. "Dad..." she said "... I've lost the key from the flat. Mom isn't home. Can I sit here with you meanwhile?". "I finish in half an hour anyways, Sheiny." said Mendel "Are you hungry?". 

The half an hour had passed and they went walking with two sandwiches made by Mendel in the morning. They were still in the industrial zone, so to speak. It was still the place of closed down little factories and rubble or rubbish. Mendel kicked an open can of beans that was laying on the road in front of them. Sheiny had noticed the expiration date on it. It said: 1998-06-23. The can twice as older then herself. "How old are you, Mendel?" she asked of him while searching frantically the answer in her own mind. "Twenty eight" was his answer.

She had told him that she went to his house but could not enter. He smiled. He no longer lived in that house. A lot of people left that building since the incident. He gave her his new address and they entered a bus. "What happened to you and your girlfriend?" she asked "You have never told me that story.". He started talking.

When he was about 15 or so years old, he and his girlfriend Cherish fell in love. He remembered her name was Cherish. She had a normal, formal name, but he didn't remember what it was. Everybody simply called her Cherish. They have been separated when he was eighteen years old. Sheiny did a quick math. It was before she was born. The reason for their separation was her father. Mendel was just a little bit older then Cherish. And when she was seventeen and he was already eighteen, her father called police on him. They were together before that for three years, he said. But her father could not get rid of him since there was no laws that were broken. Then that little period had arrived. So her father knew that this was an opportunity to get rid of Mendel. Mendel got a good trial. Instead of the usual twenty or so years, the judge gave him only ten. It was a way of saying that the judge understood the absurdity of the situation. Now he is looking for her. She was living somewhere in that neighbourhood. Maybe he gonna find her. But will he be able to recognize her? Maybe Mendel already seen her a couple of times and failed to know that it was Cherish. Maybe she is married, with kids, fat and ugly... But he still loves her.

They made it to their neighbourhood and exited the bus. Now Sheiny wanted to convince him that Cherish is either dead or doesn't love him anymore. And that he must join her little illegal show. "Mendel" she started "... well... do you want to see our shed?"

"What shed?" he asked. 

"The one where we film things." she answered nervously. Mendel didn't want to go. "Please..." begged him Sheiny. 

Mendel didn't want to go still. Mendel got sad and realized something. "Your dad..." he said "... could put me to jail. And this time it would not be for ten years. It would be for the whole twenty. I will come out a very old man. I think what is right to do is to leave you alone and never speak to you again."

"My dad left us when I was small." she said with a tear in her eye "My mother is trying to sustain me alone. But she can't. And the only way to make money is currently - the shed. The only problem is, we need a male actor. Our sales dropped. They all demand a male actor."

Sheiny started crying at Mendel. It was not a lie. Mendel listened to it and it brought a very deep thought in him. He was gone without uttering a word. Full in sadness. Full in regret. But he felt like this was the right thing to do.

Sheiny sat on the pavement where she was. She was not about to give up. But she just couldn't figure out what to do with Mendel. Sometimes he was this nice dude, welcoming and everything. The other times he was this closed-in man, with a life long trauma about his lost girlfriend. She remembered the chess board from yesterday. She knew that she just needs to come up with a better move. Mendel was now her new game of "convincing". And Sheiny was now playing it.

She came to his new house that didn't induce any kind of heart beat this time at all. She knocked on his door. He opened it and looked at her silently for about ten seconds. Then he shut the door. "Go away" he said from with in, while disappearing into the flat. Sheiny thought, okay, this didn't work. Next move. She sat outside of his house in thought of the next move, suddenly rain started. Sheiny came into the building to cover herself from the water. Suddenly something struck her.

She went right into the rain and stood there under the pouring water for several minutes. Soaking herself to the core. Then, wet as a sponge, she climbed the stairs and knocked on his door again. He opened it, saw that it was her and shut it. She thought that it was the stupidest chess move she had ever done. Suddenly the door opened again. "Are you kidding me?" he said to her almost violently "Get in.". He was not in the good mood it had seemed. But from the other side, he was barely holding himself from smiling. He gave her a huge towel from the bath and told her to wait here until she is dry. Meanwhile he went into his room, trying to ignore her.

She sat the saloon drying up, waiting that he will come out of the room and they could talk more. But then she heard him snore. He was sleeping. Perhaps, tired from work, she thought, under the influence of the rain, he couldn't help but to fall asleep. She sat for some while and then a stupid idea hit her. She took out her phone and saw a message from Chloe. "Where are you?" the message said. "I'm at Mendel's. I will probably stay here all night." Sheiny wrote "Please, if my mom calls, tell her that I will sleep in your house tonight.". She pressed "send". A few more dreadful minutes later she wrote to her mother. The message said "I'm going to sleep in Chloe's house today. Don't wait for me home.". She was unsure about what to do next. But she was sure that the wet clothes were very annoying. She removed them, covering herself only with the towel.

An hour or so passed as she was sitting stupidly in Mendel's saloon. She did nothing but contemplate her next move. Then she got up and silently went into Mendel's room. Still naked, with only a towel on her. She looked at him stupidly for perhaps another minute or two, before gaining enough strength for what she was about to do next. She entered the bed, hugged Mendel tightly and thrown out the wet towel. After a few minutes of laying like this, she too had fallen asleep.

The morning was wonderful. "What the fuck!" were the words that woke Sheiny up. Mendel was looking at her, still laying in bed and covered by the blanket. It was both frightening and somewhat relieving. Mendel didn't seem to be angry. He just was simply confused. He starred at her for a minute. This time, not searching his girlfriend in her features, but rather contemplating about what is happening. In some distant emotion, deep inside his head, he was extremely proud of himself. He took the blanket down which revealed Sheiny's naked body. Then he put the blanket back on and carefully escaped it to the side as to keep Sheiny covered. As he stood up, a bulge was visible in his trousers. This situation aroused him. He went out the room. 

"Where are you clothes?" he asked her from outside. "On the couch in the saloon." she said. "Will your mom be wondering where you were all night?" he said a bit angrily before appearing with her now dry clothes at the door. He thrown them at her and went out the room again. "Please, dress up." he said. 

"She thinks I'm at Chloe's." Sheiny told him "I asked Chloe to back it up, if she asks.". Sheiny went out the room and there was Mendel. He was not angry. More like a mixture of doomed and proud could be read on his face. It was evident that he did enjoy that surprise. 

"Are you sure that she will not call Chloe's parents instead?" he asked her in paranoia "I'm going to jail for this! This is definite! They are going to knock on my door any second! Or take me from my work. That's what's going to happen."

Sheiny thought for a second. It was a valid criticism for her plan. She didn't know what to answer immediately. "My mom is not going to be that smart," she said. "She is against being smart." Mendel looked at her with a bit of relieve. He was already familiar with such people. He sat down on the couch and looked at Sheiny with a face of doubt. Something was tormenting him. 

"Are you okay?" she asked him. 

"You look like her." he said "You like if they took her and made her tiny. It's weird. I love her. But there is no her here. She does not exist. Only you. A cheap substitute. A little girl."

"I love you." Immediately started an uncontrollable beating of her heart. 

"I guess," he said, "I cannot argue with you about the nature of love. You sell that stuff. Maybe it's even true. Maybe indeed they took her and shrunk her down and you are Cherish. I do feel, don't get me wrong, a feeling similar to love towards you. I was angry at you yesterday, I'm sorry. But please understand, I do not want to risk being in prison for another period. I just got out."

The face of the dead man appeared in front of Sheiny. Her cruelty killed him. Her brain was not very kind to Mendel at this moment. She started explaining him how they planned to keep the identity of the male actor secret, totally understanding that it might not even work. She told him about the shaving of the hairs. About the average looks, that Mendel apparently has. He refused to remove his trousers, or to tell her whether he was cut or uncut. He felt ashamed sitting there and listening to it like it was something important. He felt ashamed wanting her. It was time for his job and for her school. And to avoid further suspicions both of them must part immediately. He agreed that she can come once in a while to him. Only that it must not be sexual. She took it as a small victory. As a rope that she can hold on to, while climbing up to him. She didn't except that this rule was forever. She knew that something could be done to break his strength. The question was, what exactly?
