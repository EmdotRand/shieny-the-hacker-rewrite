# Chapter I
## Chloe

Chloe is a kind of girl that Sheiny is not going to be friends with. She is not especially smart,  but nothing is stopping her from becoming smart. Her brain works just fine. It's just Chloe was never interested in smart things. She is not a math enthusiast. She is not a programmer. She is a girl trying to be eighteen plus before being eighteen plus. 

Sheiny found Chloe sauntering near a group of chatting boys. Rather than simply pass by, she reached out and gripped the smallest of the group by the rear. He jumped, garnering a laugh from his friends. This is the kind of vulgarity that Sheiny would avoid until now. But now, she needs Chloe. She needs Chloe's expertise in making men excited. Sheiny needs better stuff to sell.

She pursued the young nymph, not knowing much what words she was going to use. To put it lightly, it's not the kind of conversation that's easy to make. Especially when you are talking to an almost stranger. By the time she caught up, Chloe had taken out her phone to type something.

"I think that you are making a mistake," said Sheiny. 

Chloe to stopped to face Shieny. She didn't understand what she was talking about. 

"Using your phone for all of it is not the safest choice," Sheiny added .

 "What do you mean?" Chloe was still confused. 
 
 "Let me see," Sheiny yanked Chloe's phone out her hand.
 
 "Hey! Give it back."
 
Shieny dodged the first grab while unlocking the phone again. She observed the password on a prior occasion. A simple turning motion keeps Chloe to her back as she swiped sideways. After a moment she found an application that she was looking for. She backed off before showing Chloe the screen. 

"What do you think this is?" asked Sheiny. The app had a friendly looking picture of a child and a mom as it's icon. 

"I don't know," answered Chloe. "It's some kind of boring thing. Now give it back." She promptly retrieved the phone. 

"This is an app that lets you're mom and dad look what you are doing with your phone. I know how to get rid of it if you want." Sheiny left Chloe to contemplate about her phone and the spy app on it. 

Later that day, after the school had ended and kids already left, Sheiny was doing some more programming in the computer class. While Mr. Hambleton was checking the computers, wearing a slightly more satisfied grin on his face this time. Suddenly a door was opened and Chloe came in. "How do I get rid of it? It's not delete-able," said Chloe while stretching her arm with the phone in it. 

"Well..." Sheiny explained that those apps are what's called "Parental Control". They are not delete-able by the child since they were designed to keep the child under the control. Those apps snoop on anything that the child does, store this information on some remote computer and then give it to the parents on request. So Chloe's parents are probably already aware of the stuff that Chloe is doing. And in order to get rid of this program, Sheiny explained, a wipe off the entire operating system should have been performed. Which required saving accounts and files on Sheiny's computer. 

It did not *have* to be performed. But while there was a chance to free Chloe completely, Sheiny felt an urge to do more then simply deleting an application. So, she convinced Chloe that the next steps are essential to stay safe. She overwritten the operating system to a one not containing a single trace of Google's spyware. Instead of Android Chloe now had Replicant on her phone. Telegram, the app she used to talk to men, still worked. And restoring the theme and the wallpaper, made Chloe feel like if though nothing had changed.

Using this opportunity, Sheiny asked Chloe more about her adventures online. She wanted to know more information of how to excite men. All of this was overheard by the busy Mr. Hambleton. 

"Are you already selling on the Dark Web?" he said.

"What?" asked Chloe back without understanding his question. 

"How do you sell the porn?" said Mr. Hambleton. This caused a minute of awkward silence. 

"She doesn't know that I sell this stuff yet." Sheiny interrupted the silence. 

9 year old Chloe looked at both of them with a confused face. "What the fuck!"

This was the kind of moment that Sheiny was afraid of doing herself. But now, that the barricade of ideas was broken, she could speak to Chloe a little bit more clearly. One downside of this situation Sheiny realized but didn't feel much through was that Mr. Hambleton was revealed to Chloe now too. Mr. Hambleton was afraid at first, but he realized that Chloe is quite a bright girl herself. And that he is not under any threat from her. "Relax, Hamb. I'm not from police," were the Chloe's words that calmed Mr. Hambleton a little. But there was a bit of calming down yet to be had by him after school to realize that he is not going to jail quite yet.

"Are you already selling on the Dark Web?" were the words repeating on a loop inside Sheiny's head. She didn't even think about it yet. The dark web is a mysterious place. It's usually a bunch of people on .onion websites doing god knows what. It could range from completely harmless things. Forums about cooking. Websites of rebellious freedom seekers living in China and other problematic areas. Image boards with memes and funny quotes. Small communities of like minded individuals. But in the same time this is a place full of websites selling drugs, weapons and all kinds of illegal pornography.

The interesting underlying part of the whole thing is the Tor Browser which connects to a network called Onion. The Onion Routing is a concept of wrapping connections into other connections to conceal anything passed through it. Essentially if you are a criminal, the only protocol good enough to conceal both your identity and the information you pass to and from the website is Onion. And the easiest way to connect to it is the Tor Browser. It was developed to help people in countries without the Freedom of Speech to get Freedom of Speech securely. But because of the free nature of the protocol it's used to do all kinds of crazy and weird things.

Sheiny was contemplating using Onion to sell her stuff. But she knew that there was a problem. If she sells online, money had to be transferred to her somehow. Using a bank transfer was not possible. At her age, having a bank account was not feasible. She could try and use BitCoin or something like this. But there was this same problem all over again. To cash out the BitCoin she would need a bank account. She could perhaps ask the people online to send her money in cash, in an envelope. A thought that she considered for a while. But there was a whole can of worms with this approach as well. For example, people on the Dark Web tent to not trust anybody.

Sheiny was about to ask another favor from Mr. Hambleton. He had a bank account after all. And she could design a way to conceal the transactions enough, so he would be left free. But as she went to speak to him, he interrupted her. 

"I have a friend that got interested in your stuff. Have you got anything with Chloe?" said Mr. Hambleton. 

With Chloe? she thought. That's a weird, but an interesting idea. But first, there was a potential new customer. And she couldn't let this opportunity go. She asked more about the mysterious friend. 

Mr. Hambleton replied with "I will show you to him next week. But he wants something with both of you." 

B-both? Sheiny and Chloe? Together? Some kind of weird mixed feeling of disgust and interest fired in her. She wanted to talk to Chloe because of information she possessed. But she never thought that Chloe might be an actress in her little illegal plays.
